// include gulp
var gulp = require('gulp');
// include plug-ins
var jshint = require('gulp-jshint');
// include plug-ins
var changed = require('gulp-changed');
// include plug-ins
var imagemin = require('gulp-imagemin');
// include plug-ins
var minifyHTML = require('gulp-minify-html');
// include plug-ins
var concat = require('gulp-concat');
// include plug-ins
var stripDebug = require('gulp-strip-debug');
// include plug-ins
var uglify = require('gulp-uglify');
// include plug-ins
var autoprefix = require('gulp-autoprefixer');
// include plug-ins
var minifyCSS = require('gulp-minify-css');
// include plug-ins
var browserSync = require('browser-sync');
// include plug-ins
var notify = require("gulp-notify");
// include plug-ins
var inject = require('gulp-inject');

gulp.task('inject', function () {
  var target = gulp.src('app/src/index.html');
  var sources = gulp.src(['app/src/**/*.js', 'app/src/styles/**/*.css'], {read: false});
  return target.pipe(inject(sources))
    .pipe(gulp.dest('app/src'));
});

// JS hint task
gulp.task('jshint', function() {
  gulp.src('app/src/scripts/**/.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// CSS hint task
gulp.task('csshint', function() {
  gulp.src('app/src/styles/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// HTML hint task
gulp.task('htmlhint', function() {
  gulp.src('app/src/views/**/*.html')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// minify new images
gulp.task('imagemin', function() {
  var imgSrc = 'app/src/images/**/*',
      imgDst = 'build/src/images';
      gulp.src(imgSrc)
          .pipe(changed(imgDst))
          .pipe(imagemin())
          .pipe(gulp.dest(imgDst));
});

// minify new or changed HTML pages
gulp.task('views', function() {
  var htmlSrc = 'app/src/views/**/*.html',
      htmlDst = 'build/src/views';
      gulp.src(htmlSrc)
        .pipe(changed(htmlDst))
        .pipe(minifyHTML())
        .pipe(gulp.dest(htmlDst));
});

// JS concat, strip debugging and minify
gulp.task('scripts', function() {
  gulp.src(['lib/lib.js','app/src/**/*.js'])
    .pipe(concat('script.js'))
    .pipe(stripDebug())
    .pipe(uglify())
    .pipe(gulp.dest('build/scripts/'));
});

// CSS concat, auto-prefix and minify
gulp.task('styles-concat-minify', function() {
  gulp.src(['app/src/styles/*.css'])
    .pipe(concat('styles.css'))
    .pipe(autoprefix('last 2 versions'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('build/src/styles/'));
});

// default gulp task
gulp.task('watch', function() {
  // watch for HTML changes
  gulp.watch('app/src/**/*.html');

  // watch for JS changes
  gulp.watch('app/src/**/*.js');

  // watch for CSS changes
  gulp.watch('app/src/**/*.css');
});

// Serve application
gulp.task('server', ['jshint', 'csshint', 'htmlhint'], function() {
  browserSync.init({
    server: {
      baseDir: 'app/src',
    },
  });
});

gulp.task('hint', ['jshint', 'csshint', 'htmlhint'], function() {

});

gulp.task('reload', ['inject'], function() {
  browserSync.reload();
});

// var bowerFiles = require('lib'),
//     inject = require('gulp-inject'),
//     stylus = require('gulp-stylus'),
//     es = require('event-stream');
//
// gulp.task('inject', function () {
//   var target = gulp.src('app/src/index.html');
//   var sources = gulp.src(['app/src/**/*.js', 'app/src/styles/**/*.css'], {read: false});
//   return target.pipe(inject(sources))
//     .pipe(gulp.dest('app/src'));
// });
//
//
// gulp.task('inject', function () {
//   var cssFiles = gulp.src('app/src/styles/**/*.css')
//     .pipe(stylus())
//     .pipe(gulp.dest('app/src'));
//
//   gulp.src('app/src/index.html')
//     .pipe(inject(gulp.src(bowerFiles(), {read: false}), {name: 'bower'}))
//     .pipe(inject(es.merge(
//       cssFiles,
//       gulp.src('app/src/app/**/*.js', {read: false})
//     )))
//     .pipe(gulp.dest('app/src'));
//
// });
