(
  function () {

    'use strict';

    angular.module('cotacaoMoedaService', []).factory('cotacaoMoedaFactory', CotacaoMoedaFactory);

    function CotacaoMoedaFactory($log, $q, $http){

      var log = $log;
      log.debug('Handler:','CotacaoMoedaFactory');

      var urlApi = "http://api.promasters.net.br/cotacao/v1/valores";

      return {
        get : function() {
          var promise = $q.defer();
          $http({
            withCredentials: false,
            method : 'get',
            url : urlApi
          })
          .then(function(response) {
            promise.resolve(response.data);
          })
          .catch(function(error) {
            return error;
          });
          return promise.promise;
        }
      }

    })();
