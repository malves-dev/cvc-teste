(
  function(){

    'use strict';

    angular.module('locadoraService', []).factory('LocadoraCarroFactory', LocadoraCarroFactory);

    function LocadoraCarroFactory($log){
      
      var LOG = $log;

      LOG.info('Handler: LocadoraCarroFactory');

      var urlApi = "http://localhost:3000/api/locacao.json";

      return {
        get : function() {
          var promise = $q.defer();
          $http({
            withCredentials: false,
            method : 'get',
            url : urlApi
          })
          .then(function(response) {
            promise.resolve(response.data);
          })
          .catch(function(error) {
            LOG.error('ERROR: LocadoraCarroFactory response ->', error);
            return error;
          });
          return promise.promise;
        }
      }
    }

  })();
