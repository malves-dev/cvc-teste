(function () {

  'use strict';

  angular.module('buscaCarroDirective', []).directive('buscaCarro', buscaCarro);

  function buscaCarro($log) {

    return {
      templateUrl: 'views/template-busca.html',
    }
  }

})();
