(function () {

  'use strict';

  angular.module('cvcLocacaoCarro', []).directive('endereco', Endereco);

  function Endereco($log){

    return {
      templateUrl: 'views/';
    }
  }

})();
