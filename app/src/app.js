

angular.module('cvcLocacaoCarro', [
  'ngRoute',
  'locacadoraController',
  'cotacaoMoedaService',
  'locadoraService',

])

.config(function($routeProvider){
    $routeProvider
    .when('/', {
      templateUrl: 'views/template-principal.html',
      controller: 'LocacaoController',
      controllerAs: 'vm'
    });
    // .otherwise({
    //     redirectTo : '/'
    // });
});
