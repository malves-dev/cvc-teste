
(function(){

    'use strict';

    angular.module('locacadoraController', ['buscaCarroDirective','locacaoCarroDirective'])
    .controller('LocacaoController', LocacaoController);

    function LocacaoController($log, LocadoraCarroFactory){
      var LOG = $log;
      LOG.info('Handler:', 'LocacaoController');

      var vm = this;
      vm.busca = false;
      vm.outroLocal = false;
      vm.buscar = buscarLocadoras;
      vm.itensPagina = [5, 10, 20, 30, 40, 50, 60, 70, 100];
      vm.ordenadoPor = ['Menor preço','Maior preço'];
      vm.titulo = ['CARRO ECONÔMICO'];
      vm.codigoTaxa = ['IFMR'];
      vm.descricao = 'CHEVROLET CELTA, FORD FIESTA, VW GOL, FIAT PÁLIO OU SIMILAR';
      vm.itensCaracteristicas = ['Quilometragem Livre', 'Seguro total do veículo', 'Seguro a terceiros', 'Taxas de serviços inclusas'];


      function buscarLocadoras(){
        console.log('Handler:', 'buscar()');
        vm.busca = true;
        vm.locadoras = [];
        LocadoraCarroFactory.get().then(function(data){
            vm.locadoras = data;
            LOG.info('Result: LocacaoController data ->', vm.locadoras);
        });
      }

      //buscar();

    }

  })();
